CREATE DATABASE IF NOT EXISTS `HsExtrasDB`;

CREATE TABLE IF NOT EXISTS `groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf16;

CREATE TABLE IF NOT EXISTS `ips` (
  `tipo` varchar(5) NOT NULL,
  `address` varchar(15) NOT NULL,
  PRIMARY KEY (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rol` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf16;

CREATE TABLE IF NOT EXISTS `usuarios` (
  `usuario` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

CREATE TABLE `registros` (
  `fecha` datetime NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `user_ipaddress` varchar(15) NOT NULL,
  `tcp_ipaddress` varchar(15) NOT NULL,
  `conn_duration` time NOT NULL,
  `bytes_in` bigint(20) unsigned NOT NULL,
  `bytes_out` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fecha`,`usuario`,`user_ipaddress`,`tcp_ipaddress`),
  KEY `FK_tcp_address` (`user_ipaddress`),
  KEY `FK_public_address` (`tcp_ipaddress`),
  KEY `FK_usuarios` (`usuario`),
  CONSTRAINT `FK_public_address` FOREIGN KEY (`tcp_ipaddress`) REFERENCES `ips` (`address`) ON DELETE CASCADE,
  CONSTRAINT `FK_tcp_address` FOREIGN KEY (`user_ipaddress`) REFERENCES `ips` (`address`) ON DELETE CASCADE,
  CONSTRAINT `FK_usuarios` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`usuario`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

CREATE TABLE `rel_group_usuario` (
  `idUsuario` varchar(100) NOT NULL,
  `idGrupo` bigint(20) NOT NULL,
  `rol_id` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FK_rol` (`rol_id`),
  KEY `FK_grupo` (`idGrupo`),
  KEY `FK_usuario` (`idUsuario`),
  CONSTRAINT `FK_grupo` FOREIGN KEY (`idGrupo`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_rol` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`usuario`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16;




DELIMITER //
CREATE TRIGGER populateTables BEFORE INSERT
ON `registros` FOR EACH ROW
BEGIN 
  IF NOT EXISTS 
  (SELECT * FROM usuarios WHERE usuario=NEW.usuario) THEN INSERT INTO usuarios(usuario) VALUES (NEW.usuario); 
  END IF;
	IF NOT EXISTS 
  (SELECT * FROM ips WHERE address=NEW.tcp_ipaddress) THEN INSERT INTO ips(address,tipo) VALUES (NEW.tcp_ipaddress,'TCP');
  END IF;
	IF NOT EXISTS (SELECT * FROM ips WHERE address=NEW.user_ipaddress) THEN INSERT INTO ips(address,tipo) VALUES (NEW.user_ipaddress,'PUB');
  END IF;
END; //
DELIMITER;


select
    `registros`.`usuario` AS `usuario`,
    cast(`registros`.`fecha` as date) AS `Fecha de Conexión`,
    date_format(min(`registros`.`fecha`), '%H:%i:%s') AS `Inicio de Sesión`,
    max(`registros`.`conn_duration`) AS `Duración de la conexión (log)`,
    sec_to_time((count(`registros`.`usuario`) * 60)) AS `Conexion Exacta`,
    max(`registros`.`bytes_in`) AS `Subido en Bytes`,
    round(((max(`registros`.`bytes_in`) / 1024) / 1024), 2) AS `MB Subidos`,
    max(`registros`.`bytes_out`) AS `Bajado en Bytes`,
    round(((max(`registros`.`bytes_out`) / 1024) / 1024), 2) AS `MB Bajados`
from
    `registros`
group by
    cast(`registros`.`fecha` as date),
    `registros`.`usuario`
order by
    cast(`registros`.`fecha` as date)

IF NOT EXISTS (SELECT * FROM ips WHERE address=NEW.user_ipaddress) THEN INSERT INTO ips(address,tipo) VALUES (NEW.user_ipaddress,'PUB');
IF NOT EXISTS 
(SELECT * FROM registros WHERE CAST(NEW.fecha as DATE) = CAST(OLD.fecha)+1 and OLD.usuario=NEW.usuario THEN
 INSERT INTO ips(address,tipo) VALUES (NEW.user_ipaddress,'PUB');