Script actualConnections.py

Creado para leer crear la lista de usuarios que estan conectados ahora a la VPN
Campos:

* Fecha Hora de consulta snmp
* usuario AD
* IP Publica
* IP Local del Tribunal de Cuentas
* Tiempo que ha durado la conexion sin cortes
* Megabytes recibidos (Subida del Usuario)
* Megabytes enviados (Descarga del Usuario)


Script pySnmpReport.py

Creado para generar un registro de las conexiones de la VPN.

* Fecha Hora de consulta snmp
* usuario AD
* IP Publica
* IP Local del Tribunal de Cuentas
* Tiempo de conexion en segundos
* Tiempo que ha durado la conexion sin cortes en HH:MM:SS
* Bites subiedos (Megabytes recibidos (Subida del Usuario))
* Bites bajados (Megabytes enviados (Descarga del Usuario))

Script vpnwatcher.py

tiene las funciones de pySnmpReport.py. Solo que almacena los datos en una base de datos

[ ] Opcion Housekeeping
[ ] Housekeeping almacenando las conexiones diarias en otra tabla para evitar el uso de la vista
