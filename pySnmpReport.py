#-*- coding: utf-8 -*-
from os import spawnl
import shlex
import subprocess
import requests
import json
import logging
import time
import rlcompleter
import readline
import datetime

"""
FgVpnSslTunnelEntry ::= SEQUENCE {
    fgVpnSslTunnelIndex     FnIndex,
    fgVpnSslTunnelVdom      FgVdIndex,
    fgVpnSslTunnelUserName  DisplayString, puede ser HEX-STRING
    fgVpnSslTunnelSrcIp     IpAddress,
    fgVpnSslTunnelIp        IpAddress,
    fgVpnSslTunnelUpTime    Counter32,
    fgVpnSslTunnelBytesIn   Counter64,
    fgVpnSslTunnelBytesOut  Counter64
"""

logging.basicConfig(filename='/home/TRIBUNALCBA/dpaniagua/zabbix-scripts/pysnmp.log', filemode='a+', level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S', format='%(asctime)s - %(message)s')

from requests.api import request
oid = '1.3.6.1.4.1.12356.101.12.2.4.1'
command = f'snmpwalk -v2c -c 4Od1S4SQaPgrN2DNgssa 172.16.0.1 {oid}'

result = subprocess.getoutput(command)
'''
resultado = result.split('\n')[17]
.split(':')[-1][2:-1] if result.split('\n')[18].split(':')[-1][1] == '"' else bytes.fromhex(result.split('\n')[16].split(':')[-1][1:-1].replace(' ','')).decode('utf-8')

result = 'iso.3.6.1.4.1.12356.101.12.2.4.1.1.1 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.1.2 = INTEGER: 2\niso.3.6.1.4.1.12356.101.12.2.4.1.1.3 = INTEGER: 3\niso.3.6.1.4.1.12356.101.12.2.4.1.1.4 = INTEGER: 4\niso.3.6.1.4.1.12356.101.12.2.4.1.1.5 = INTEGER: 5\niso.3.6.1.4.1.12356.101.12.2.4.1.1.6 = INTEGER: 6\niso.3.6.1.4.1.12356.101.12.2.4.1.2.1 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.2.2 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.2.3 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.2.4 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.2.5 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.2.6 = INTEGER: 1\niso.3.6.1.4.1.12356.101.12.2.4.1.3.1 = STRING: "jpgerbaldo"\niso.3.6.1.4.1.12356.101.12.2.4.1.3.2 = STRING: "dpaniagua"\niso.3.6.1.4.1.12356.101.12.2.4.1.3.3 = STRING: "dmarcuzzi"\niso.3.6.1.4.1.12356.101.12.2.4.1.3.4 = STRING: "jmicheletti"\niso.3.6.1.4.1.12356.101.12.2.4.1.3.5 = Hex-STRING: 6D 70 65 C3 B1 61 \niso.3.6.1.4.1.12356.101.12.2.4.1.3.6 = STRING: "lbiasoni"\niso.3.6.1.4.1.12356.101.12.2.4.1.4.1 = IpAddress: 181.238.68.134\niso.3.6.1.4.1.12356.101.12.2.4.1.4.2 = IpAddress: 181.231.153.82\niso.3.6.1.4.1.12356.101.12.2.4.1.4.3 = IpAddress: 186.124.142.74\niso.3.6.1.4.1.12356.101.12.2.4.1.4.4 = IpAddress: 24.232.211.13\niso.3.6.1.4.1.12356.101.12.2.4.1.4.5 = IpAddress: 170.51.101.252\niso.3.6.1.4.1.12356.101.12.2.4.1.4.6 = IpAddress: 181.165.64.220\niso.3.6.1.4.1.12356.101.12.2.4.1.5.1 = IpAddress: 10.212.134.10\niso.3.6.1.4.1.12356.101.12.2.4.1.5.2 = IpAddress: 10.212.134.11\niso.3.6.1.4.1.12356.101.12.2.4.1.5.3 = IpAddress: 10.212.134.14\niso.3.6.1.4.1.12356.101.12.2.4.1.5.4 = IpAddress: 10.212.134.18\niso.3.6.1.4.1.12356.101.12.2.4.1.5.5 = IpAddress: 10.212.134.27\niso.3.6.1.4.1.12356.101.12.2.4.1.5.6 = IpAddress: 10.212.134.36\niso.3.6.1.4.1.12356.101.12.2.4.1.6.1 = Counter32: 534\niso.3.6.1.4.1.12356.101.12.2.4.1.6.2 = Counter32: 433\niso.3.6.1.4.1.12356.101.12.2.4.1.6.3 = Counter32: 7139\niso.3.6.1.4.1.12356.101.12.2.4.1.6.4 = Counter32: 7438\niso.3.6.1.4.1.12356.101.12.2.4.1.6.5 = Counter32: 9965\niso.3.6.1.4.1.12356.101.12.2.4.1.6.6 = Counter32: 27363\niso.3.6.1.4.1.12356.101.12.2.4.1.7.1 = Counter64: 310762\niso.3.6.1.4.1.12356.101.12.2.4.1.7.2 = Counter64: 1199129\niso.3.6.1.4.1.12356.101.12.2.4.1.7.3 = Counter64: 1520363\niso.3.6.1.4.1.12356.101.12.2.4.1.7.4 = Counter64: 1290610\niso.3.6.1.4.1.12356.101.12.2.4.1.7.5 = Counter64: 25762198\niso.3.6.1.4.1.12356.101.12.2.4.1.7.6 = Counter64: 10634010\niso.3.6.1.4.1.12356.101.12.2.4.1.8.1 = Counter64: 315483\niso.3.6.1.4.1.12356.101.12.2.4.1.8.2 = Counter64: 3974542\niso.3.6.1.4.1.12356.101.12.2.4.1.8.3 = Counter64: 8641238\niso.3.6.1.4.1.12356.101.12.2.4.1.8.4 = Counter64: 1341513\niso.3.6.1.4.1.12356.101.12.2.4.1.8.5 = Counter64: 12853343\niso.3.6.1.4.1.12356.101.12.2.4.1.8.6 = Counter64: 13967316'

intermediateArray
[[OID_SPLIT(.)],OID[-5:-1]]
[['iso', '3', '6', '1', '4', '1', '12356', '101', '12', '2', '4', '1', '1', '1'], '2.4.1.1', '1', '1', '1'], [['iso', '3', '6', '1', '4', '1', '12356', '101', '12', '2', '4', '1', '1', '2'], '2.4.1.1', '1', '2', '2']
'''
totalUsuarios = len([[username] for username in result.split('\n') if username[-1][-1] == '"'])

cleanUser = lambda u: u if u[-1] != '"' else u[1:-1]



intermediateArray = [
    [
        index.split(' ')[0].split('.'),
        '.'.join(index.split(' ')[0].split('.')[-5:-1]),
        index.split(' ')[0].split('.')[-2],
        index.split(' ')[0].split('.')[-1],
        #devuelve el ultimo item, este puede ser un usuario, un valor o hexadecimal
        # si es usuario clanUser lo limpia sino retorna la cadena original
        # si es valor lo deja como esta
        # si es hexa concatena las cadenas de 2 caracteres que estan separados por espacio hasta el final.
        cleanUser(index.split()[-1]) if len(index.split())==4 else  bytes.fromhex(''.join(index.split()[3:])).decode('utf-8') 
    ]
    for index in result.split('\n')
]

vpnArray = []
for i in range(1,totalUsuarios+1):
    vpnUser = []
    for item in filter(lambda x:x[-2]==f'{i}', intermediateArray):
        vpnUser.append(item[-1])
    vpnArray.append(vpnUser)
    logging.info(f'[{time.localtime()[3]*60+time.localtime()[4]}/1440] - {vpnUser[2]}\t{vpnUser[3]}\t{vpnUser[4]}\t{vpnUser[5]}\t{datetime.timedelta(seconds=int(vpnUser[5]))}\t{vpnUser[6]} B In ({(float(vpnUser[6])/1000000):.6f} MB)\t{vpnUser[7]} B Out ({(float(vpnUser[7])/1000000):.6f} MB)')

vpnLocationArray = []
#for ip in vpnArray:
#    request = requests.get(f'https://ip-api.io/api/json/{ip[3]}')
#    #print(json.loads(request.content))
#    vpnLocationArray.append([ip[2],ip[3],json.loads(request.content)["country_name"],json.loads(request.content)["region_name"]])
#    print(f'User: {ip[2]} IP Publica: {ip[3]} Pais: {json.loads(request.content)["country_name"]} Region: {json.loads(request.content)["region_name"]}')
#ipstackAPI='02145b3ef1578e8437c0c25839919f19'
#for ip in vpnArray:
#    request = requests.get(f'http://api.ipstack.com/{ip[3]}?access_key={ipstackAPI}')
#    vpnLocationArray.append([ip[2],ip[3],json.loads(request.content)["country_name"],json.loads(request.content)["region_name"],json.loads(request.content)["city"]])
#    logging.info(f'User: {ip[2]} IP Publica: {ip[3]} Pais: {json.loads(request.content)["country_name"]} Region: {json.loads(request.content)["region_name"]} Ciudad: {json.loads(request.content)["city"]}')