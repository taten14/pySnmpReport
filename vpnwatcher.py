import mysql
import subprocess
import requests
import json
import logging
import time
import rlcompleter
import readline
import datetime
from requests.api import request
from datetime import datetime as dt


import mysql.connector
from mysql.connector import Error

logging.basicConfig(filename='/home/TRIBUNALCBA/dpaniagua/zabbix-scripts/vpnwatcher.log', filemode='a+', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S', format='%(asctime)s - %(message)s')

def setConn():
    try:
        connection = mysql.connector.connect(host='localhost',
                                            database='HsExtrasDB',
                                            user='extrasdbadmin',
                                            password='d1TZx1147rGHXg0SV2UK')
        return connection
    except Exception as e:
        logging.error(f'Error: {e}\nMensaje:{type(e)}')


def insertRecord(fechaConexion, usuarioSnmp, userIP, VPNIP, tiempoConexionLog,bytesIn, bytesOut):
    try:
        connection = setConn()
        #connection = mysql.connector.connect(host='localhost',
        #                                    database='HsExtrasDB',
        #                                    user='extrasdbadmin',
        #                                    password='d1TZx1147rGHXg0SV2UK')
        query = f'INSERT INTO registros (fecha, usuario, user_ipaddress, tcp_ipaddress, conn_duration, bytes_in, bytes_out) VALUES (\'{fechaConexion}\', \'{usuarioSnmp}\', \'{userIP}\', \'{VPNIP}\', \'{tiempoConexionLog}\', \'{bytesIn}\', \'{bytesOut}\')'
        logging.info(query)
        cursor = connection.cursor()
        #cursor.execute(query)
        cursor.callproc('insertRecord',[fechaConexion, usuarioSnmp, userIP, VPNIP, tiempoConexionLog,bytesIn, bytesOut])
        connection.commit()
    except Error as e:
        logging.error(f'Error: {e}\nMensaje:{type(e)}\n{query}')
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            logging.info("MySQL connection is closed")


def getUltimaConexion(usuario):
    try:
        connection = mysql.connector.connect(host='localhost',
                                            database='HsExtrasDB',
                                            user='extrasdbadmin',
                                            password='d1TZx1147rGHXg0SV2UK')
        query = f'SELECT MAX(CAST(fecha AS DATE)) FROM registros WHERE usuario = \'{usuario}\';'
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
    except Exception as e:
        logging.error(f'Error: {e}\nMensaje:{type(e)}\n{query}')
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            logging.info("MySQL connection is closed")


def guardarDiario(fecha):
    '''
    Requiere el dia a analizar
    Busca los datos de un dia y genera el acumulado para ese dia en conexionesDiarias
    Una vez corrido almacena la fecha en diaAcumuladoProcesado
    '''
    try:
        args = [fecha]
        connection = mysql.connector.connect(host='localhost',
                                            database='HsExtrasDB',
                                            user='extrasdbadmin',
                                            password='d1TZx1147rGHXg0SV2UK')
        cursor = connection.cursor()
        cursor.callproc('populateConexionesDiarias',args)
        connection.commit()
        logging.info(f'{cursor.stored_results()}')
        setDiaAcumuladoProcesado(fecha)
    except Exception as e:
        logging.error(f'Error: {e}\nMensaje:{type(e)}')
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            logging.info("MySQL connection is closed")

def setDiaAcumuladoProcesado(fecha):
    now = dt.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
    try:
        connection = mysql.connector.connect(host='localhost',
                                            database='HsExtrasDB',
                                            user='extrasdbadmin',
                                            password='d1TZx1147rGHXg0SV2UK')
        cursor = connection.cursor()
        cursor.callproc('setDiaAcumuladoProcesado',[fecha,dt_string])
        connection.commit()
    except Exception as e:
        logging.error(f'Error: {e}\nMensaje:{type(e)}')
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            logging.info("MySQL connection is closed")











oid = '1.3.6.1.4.1.12356.101.12.2.4.1'
command = f'snmpwalk -v2c -c 4Od1S4SQaPgrN2DNgssa 172.16.0.1 {oid}'
result = subprocess.getoutput(command)

totalUsuarios = len([[username] for username in result.split('\n') if username[-1][-1] == '"'])
cleanUser = lambda u: u if u[-1] != '"' else u[1:-1]

intermediateArray = [
    [
        index.split(' ')[0].split('.'),
        '.'.join(index.split(' ')[0].split('.')[-5:-1]),
        index.split(' ')[0].split('.')[-2],
        index.split(' ')[0].split('.')[-1],
        #devuelve el ultimo item, este puede ser un usuario, un valor o hexadecimal
        # si es usuario clanUser lo limpia sino retorna la cadena original
        # si es valor lo deja como esta
        # si es hexa concatena las cadenas de 2 caracteres que estan separados por espacio hasta el final.
        cleanUser(index.split()[-1]) if len(index.split())==4 else  bytes.fromhex(''.join(index.split()[3:])).decode('utf-8') 
    ]
    for index in result.split('\n')
]

#logging.DEBUG(intermediateArray)

vpnArray = []
for i in range(1,totalUsuarios+1):
    vpnUser = []
    for item in filter(lambda x:x[-2]==f'{i}', intermediateArray):
        vpnUser.append(item[-1])
    vpnArray.append(vpnUser)
    now = dt.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
    logging.info(f'{vpnUser[2]}\t{vpnUser[3]}\t{vpnUser[4]}\t{datetime.timedelta(seconds=int(vpnUser[5]))}\t{(float(vpnUser[6])/1000000):.6f} MBIn\t{(float(vpnUser[7])/1000000):.6f} MBOut')
    print(dt_string)
    print(vpnUser[2])
    print(vpnUser[3])
    print(vpnUser[4])
    print(vpnUser[5])
    print(vpnUser[6])
    print(vpnUser[7])
    insertRecord(dt_string, vpnUser[2], vpnUser[3], vpnUser[4], datetime.timedelta(seconds=int(vpnUser[5])), vpnUser[6], vpnUser[7])
    logging.info(f'Registro: {dt_string}, {vpnUser[2]}, {vpnUser[3]}, {vpnUser[4]}, {datetime.timedelta(seconds=int(vpnUser[5]))}, {vpnUser[6]}, {vpnUser[7]}')
    















